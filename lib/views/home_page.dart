import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:timeago/timeago.dart' as timeago;
// import 'package:testapp/model/postmodel.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:connectivity_plus/connectivity_plus.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // // We will fetch data from this Rest api
  // final _baseUrl = 'https://jsonplaceholder.typicode.com/posts';
  //
  // // At the beginning, we fetch the first 20 posts
  // int _page = 0;
  // // you can change this value to fetch more or less posts per page (10, 15, 5, etc)
  // final int _limit = 20;
  //
  // // There is next page or not
  // bool _hasNextPage = true;
  //
  // // Used to display loading indicators when _firstLoad function is running
  // bool _isFirstLoadRunning = false;
  //
  // // Used to display loading indicators when _loadMore function is running
  // bool _isLoadMoreRunning = false;
  //
  // // This holds the posts fetched from the server
  List _posts = [];
  bool loading = false;
  //
  // // This function will be called when the app launches (see the initState function)
  // void _firstLoad() async {
  //   setState(() {
  //     _isFirstLoadRunning = true;
  //   });
  //   try {
  //     final res =
  //     await http.get(Uri.parse("$_baseUrl?_page=$_page&_limit=$_limit"));
  //     setState(() {
  //       _posts = json.decode(res.body);
  //     });
  //   } catch (err) {
  //     if (kDebugMode) {
  //       print('Something went wrong');
  //     }
  //   }
  //
  //   setState(() {
  //     _isFirstLoadRunning = false;
  //   });
  // }
  //
  // // This function will be triggered whenver the user scroll
  // // to near the bottom of the list view
  // void _loadMore() async {
  //   if (_hasNextPage == true &&
  //       _isFirstLoadRunning == false &&
  //       _isLoadMoreRunning == false &&
  //       _controller.position.extentAfter < 300) {
  //     setState(() {
  //       _isLoadMoreRunning = true; // Display a progress indicator at the bottom
  //     });
  //     _page += 1; // Increase _page by 1
  //     try {
  //       final res =
  //       await http.get(Uri.parse("$_baseUrl?_page=$_page&_limit=$_limit"));
  //
  //       final List fetchedPosts = json.decode(res.body);
  //       if (fetchedPosts.isNotEmpty) {
  //         setState(() {
  //           _posts.addAll(fetchedPosts);
  //         });
  //       } else {
  //         // This means there is no more data
  //         // and therefore, we will not send another GET request
  //         setState(() {
  //           _hasNextPage = false;
  //         });
  //       }
  //     } catch (err) {
  //       if (kDebugMode) {
  //         print('Something went wrong!');
  //       }
  //     }
  //
  //     setState(() {
  //       _isLoadMoreRunning = false;
  //     });
  //   }
  // }

  String image_url = "";


  String order = 'recent';
  int lpid = 0;
  int plid =0;
  int page_size = 5;
  var items = [
    'recent',
    'oldest',
  ];

  @override
  void initState() {
    super.initState();
    // _firstLoad();
checkConnection();
   
    // _controller = ScrollController()..addListener(_loadMore);
  }


  Future<void> checkConnection() async {
    var result = await Connectivity().checkConnectivity();
    if(result == ConnectivityResult.none){
    print("No internet connection");
    showDialog(context: context, builder: (context) {
      return new SimpleDialog(
       children: <Widget>[
            new Center(child: new Container(child: new Text('No internet Connection')))
       ]);
    });
}else{
 createpost();
}
  }

  @override
  void dispose() {
    // _controller.removeListener(_loadMore);
    super.dispose();
  }

  Future<void> createpost()  async{
    setState(() {
     loading = true; 
    });
    var url = Uri.https('app-test.rr-qa.seasteaddigital.com', '/api/v1/posts/feed/global.json');
    var body = {
        "page_size":page_size.toString(),
        "order": order, 
        "lpid": lpid.toString()
    
    };
    print(body);
    var header = {
      "X-APP-AUTH-TOKEN":"505ef37b7552a2db0af9e4d924daff77f754b12212a2189f7d7f833641aba693",
      "X-DEVICE-ID":"7789e3ef-c87f-49c5-a2d3-5165927298f0"
    };

     var response = await http.post(url,headers: header,body: body);

print(response.statusCode);
     if (response.statusCode == 200) {
    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;
      // print(jsonResponse);
    setState((){
      _posts = jsonResponse['data'];
    });
    var itemCount = jsonResponse['totalItems'];

  } else {
    print('Request failed with status: ${response.statusCode}.');
  }
    setState(() {
     loading = false; 
    });
  }



  String removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(
      r"<[^>]*>",
      multiLine: true,
      caseSensitive: true
    );

    return htmlText.replaceAll(exp, '');
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text(''),
        actions: [
          Row(children: [
            Text("Sort By:",style: TextStyle(color: Colors.white60),),
            DropdownButton(

              // Initial Value
              value: order,

              // Down Arrow Icon
              icon: const Icon(Icons.keyboard_arrow_down),

              // Array list of items
              items: items.map((String items) {
                return DropdownMenuItem(
                  value: items,
                  child: Text(items,style: TextStyle(color: Colors.grey),),
                );
              }).toList(),
              // After selecting the desired option,it will
              // change button value to selected value
              onChanged: (String? newValue) async {
                setState(() {
                  order = newValue!;
                });
                await createpost();
              },
            ),
          ],)
        ],
      ),
      body:
      loading
          ?
        const Center(
        child: const CircularProgressIndicator(),
      )
          :
      Column(
        children: [
          Flexible(
            child: ListView.builder(
              // controller: _controller,
              itemCount: _posts.length,
              itemBuilder: (_, index) => Card(
                color: Colors.black12,
                margin: const EdgeInsets.symmetric(
                    vertical: 8, horizontal: 10),
                child: Column(
                  children: [
                    ListTile(
                      leading:  Container(
                        width: 70,
                        height: 70,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Colors.white,
                            width: 2,
                          ),
                          image: _posts[index]['author_avatar_url'] != ""
                              ? DecorationImage(
                            image: NetworkImage(_posts[index]['author_avatar_url']),
                            fit: BoxFit.fill,
                          )
                              : DecorationImage(
                            image: AssetImage(
                                "assets/images/default.png"),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      trailing: Icon(Icons.bookmark_added_rounded,color: Colors.red,),
                      title: Text(_posts[index]['author_name'],
                      style: TextStyle(color: Colors.red),),
                      subtitle: Text(timeago.format(DateTime.fromMillisecondsSinceEpoch(_posts[index]['timestamp']*1000)).toString(),style: TextStyle(color: Colors.grey),),
                    ),

                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: SizedBox(width:300, child:HtmlWidget(_posts[index]['title']) 
                          // Text(_posts[index]['title'],style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.bold),)
                          ),),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: SizedBox( width: 300, child: Text(Bidi.stripHtmlIfNeeded(_posts[index]['text']),style: TextStyle(color: Colors.white,fontSize: 18,))),),
                      ],
                    ),
                    Divider(color: Colors.grey,),
                    Row(
                      children: [
                        SizedBox(width: 20,),
                        Icon(Icons.thumb_up_off_alt),
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Text(_posts[index]['total_post_views'].toString()),),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
          padding: EdgeInsets.fromLTRB(170.0, 0.0, 30.0, 0.0),  
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            (lpid !=0)?
            InkWell(
              onTap:(){
                setState(() {
                  page_size -= page_size;
                  lpid =  plid;
                });
                createpost();
              },
              child: Container(
                color: Colors.blue[50],
                child: Align(
                  alignment: Alignment.topRight,
                  child: Icon( Icons.arrow_back_outlined, ),
                ),
              ),
            ):Container(),
            SizedBox(width: 20,),
            InkWell(
              onTap:(){
                 setState(() {
                  page_size += page_size;
                  plid = lpid;
                  lpid =  _posts[_posts.length-1]['id'];
                  
                });
                createpost();
              },
              child: Container(
                color: Colors.blue[50],
                child: Align(
                  alignment: Alignment.topRight,
                  child: Icon( Icons.arrow_forward_outlined, ),
                ),
              ),
            ),
    ],
  ),
)
          // // when the _loadMore function is running
          // if (_isLoadMoreRunning == true)
          //   const Padding(
          //     padding: EdgeInsets.only(top: 10, bottom: 40),
          //     child: Center(
          //       child: CircularProgressIndicator(),
          //     ),
          //   ),
          //
          // // When nothing else to load
          // if (_hasNextPage == false)
            // Container(
            //   padding: const EdgeInsets.only(top: 30, bottom: 40),
            //   color: Colors.amber,
            //   child: const Center(
            //     child: Text('You have fetched all of the content'),
            //   ),
            // ),
        ],
      ),
    );
  }
}